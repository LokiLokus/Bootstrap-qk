# Bootstrap-qk

Basiclly just use at like normal Bootstrap and add to certain classes `-qk`.

## Buttons

To make a flat styled Button just add the `btn-qk` class.

```html
<button class="btn-qk btn-primary">Flat</button>
```
